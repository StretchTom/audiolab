package edu.umass.cs.client;

/**
 * Created by tmutabazi on 10/8/2015.
 */
public class SpeechClassifier {



    public static double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = SpeechClassifier.N3586eba60(i);
        return p;
    }
    static double N3586eba60(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 99.97939808681781) {
            p = SpeechClassifier.N2e0469ae1(i);
        } else if (((Double) i[0]).doubleValue() > 99.97939808681781) {
            p = SpeechClassifier.N5cf86b404(i);
        }
        return p;
    }
    static double N2e0469ae1(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 0;
        } else if (((Double) i[4]).doubleValue() <= 3.9737705847379194) {
            p = SpeechClassifier.N35b24a922(i);
        } else if (((Double) i[4]).doubleValue() > 3.9737705847379194) {
            p = 0;
        }
        return p;
    }
    static double N35b24a922(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 0.5525622548130666) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() > 0.5525622548130666) {
            p = SpeechClassifier.N5e2170f93(i);
        }
        return p;
    }
    static double N5e2170f93(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 7.416500917761119) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() > 7.416500917761119) {
            p = 0;
        }
        return p;
    }
    static double N5cf86b404(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 0.061700602965951035) {
            p = SpeechClassifier.N513c20685(i);
        } else if (((Double) i[1]).doubleValue() > 0.061700602965951035) {
            p = SpeechClassifier.N126ff1f310(i);
        }
        return p;
    }
    static double N513c20685(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 104.66908875364689) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 104.66908875364689) {
            p = SpeechClassifier.N76fd16076(i);
        }
        return p;
    }
    static double N76fd16076(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= -1.2053286441598567) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() > -1.2053286441598567) {
            p = SpeechClassifier.N238e06997(i);
        }
        return p;
    }
    static double N238e06997(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() <= -2.225011696172365) {
            p = SpeechClassifier.N3c33dbc38(i);
        } else if (((Double) i[9]).doubleValue() > -2.225011696172365) {
            p = 1;
        }
        return p;
    }
    static double N3c33dbc38(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 1;
        } else if (((Double) i[9]).doubleValue() <= -3.832644558796681) {
            p = SpeechClassifier.Ncbc934d9(i);
        } else if (((Double) i[9]).doubleValue() > -3.832644558796681) {
            p = 0;
        }
        return p;
    }
    static double Ncbc934d9(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 112.2484432952556) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 112.2484432952556) {
            p = 1;
        }
        return p;
    }
    static double N126ff1f310(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() <= 6.267801964263376) {
            p = 1;
        } else if (((Double) i[3]).doubleValue() > 6.267801964263376) {
            p = SpeechClassifier.N410697e911(i);
        }
        return p;
    }
    static double N410697e911(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 1.6311947147632013) {
            p = SpeechClassifier.N63c9b1c912(i);
        } else if (((Double) i[1]).doubleValue() > 1.6311947147632013) {
            p = 1;
        }
        return p;
    }
    static double N63c9b1c912(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() <= 0.2442459815568787) {
            p = 1;
        } else if (((Double) i[1]).doubleValue() > 0.2442459815568787) {
            p = 0;
        }
        return p;
    }
}



