package edu.umass.cs.client;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.audioClasses.FeatureExtractor;
import com.audioClasses.MicrophoneRecorder;
import com.meapsoft.FFT;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;

import edu.umass.cs.accelerometer.Filter;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * 
 * Context_Service: This is a sample class to reads sensor data (accelerometer). 
 * 
 * @author CS390MB
 * 
 */
public class Context_Service extends Service implements SensorEventListener,MicrophoneRecorder.MicrophoneListener{

	/**
	 * Notification manager to display notifications
	 */
	private NotificationManager nm;
	
	/**
	 * SensorManager.
	 */
	private SensorManager mSensorManager;
    /**
     * Accelerometer Sensor
     */
    private Sensor mAccelerometer;
    private Sensor mMicrophone;

	//List of bound clients/activities to this service
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    ArrayList<Double> filteredNumbers = new ArrayList<Double>();
    ArrayList<Double> Numbers = new ArrayList<Double>();
    private static ArrayBlockingQueue<Double> mAccBuffer;
    private Instances mDataset;
    private Attribute mClassAttribute;
    private static final int mFeatLen = Globals.ACCELEROMETER_BLOCK_CAPACITY + 1;
    private String mLabel;
    private int mServiceTaskType;
    private OnSensorChangedTask mAsyncTask;


	//Message codes sent and received by the service
	static final int MSG_REGISTER_CLIENT = 1;
	static final int MSG_UNREGISTER_CLIENT = 2;
	static final int MSG_ACTIVITY_STATUS = 3;
	static final int MSG_STEP_COUNTER = 4;
	static final int MSG_ACCEL_VALUES = 5;
	static final int MSG_START_ACCELEROMETER = 6;
	static final int MSG_STOP_ACCELEROMETER = 7;
	static final int MSG_ACCELEROMETER_STARTED = 8;
	static final int MSG_ACCELEROMETER_STOPPED = 9;
    //static final int CURRENT_ACTIVITY = 10;
    static final int MSG_START_MICROPHONE = 10;
    static final int MSG_STOP_MICROPHONE = 11;
    static final int MSG_MICROPHONE_STARTED = 12;
    static final int MSG_MICROPHONE_STOPPED = 13;
    static final int MSG_MICROPHONE_STATUS = 14;

	static Context_Service sInstance = null;
	private static boolean isRunning = false;
	private static boolean isAccelRunning = false;
    private static boolean isMicrophoneRunning = false;
	private static final int NOTIFICATION_ID = 777;
    private int currentState = -1;
    private int previousState = -1;
    private int increment = 0;
    private Object[] result = new Object[12];
	
	/**
	 * Filter class required to filter noise from accelerometer
	 */
	private Filter filter = null;
	/**
	 * Step count to be displayed in UI
	 */
	private int stepCount = 0;
    private int speechNumber= -1;
	
	//Messenger used by clients
	final Messenger mMessenger = new Messenger(new IncomingHandler());
    MicrophoneRecorder recorder = MicrophoneRecorder.getInstance();


	/**
	 * Handler to handle incoming messages
	 */
	@SuppressLint("HandlerLeak")
	class IncomingHandler extends Handler { 
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;
			case MSG_UNREGISTER_CLIENT:
				mClients.remove(msg.replyTo);
				break;
			case MSG_START_ACCELEROMETER:
			{
				isAccelRunning = true;
				mSensorManager.registerListener(sInstance, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
				sendMessageToUI(MSG_ACCELEROMETER_STARTED);
				showNotification();
				//Set up filter
				//Following sets up smoothing filter from mcrowdviz
				int SMOOTH_FACTOR = 10;
				filter = new Filter(SMOOTH_FACTOR);
				//OR Use Butterworth filter from mcrowdviz
				//double CUTOFF_FREQUENCY = 0.3;
				//filter = new Filter(CUTOFF_FREQUENCY);
				stepCount = 0;
				break;
			}
			case MSG_STOP_ACCELEROMETER:
			{
				isAccelRunning = false;
				mSensorManager.unregisterListener(sInstance);
				sendMessageToUI(MSG_ACCELEROMETER_STOPPED);
				showNotification();
				//Free filter and step detector
				filter = null;

				break;
			}
                case MSG_START_MICROPHONE:
                {
                    recorder = MicrophoneRecorder.getInstance();
                    if(!recorder.isRecording()) {
                        isMicrophoneRunning = true;
                        recorder.registerListener(getInstance());
                        recorder.startRecording();
                        sendMessageToUI(MSG_MICROPHONE_STARTED);
                        showNotification();
                    }

                    //Free filter and step detector


                    break;
                }
                case MSG_STOP_MICROPHONE:
                {
                    recorder = MicrophoneRecorder.getInstance();
                    if(recorder.isRecording())
                    {
                        isMicrophoneRunning = false;
                        recorder.stopRecording();
                        sendMessageToUI(MSG_MICROPHONE_STOPPED);
                        showNotification();
                    }

                    break;
                }
                case MSG_MICROPHONE_STATUS:
                {

                    sendMessageToUI(MSG_MICROPHONE_STATUS);
                    break;
                }
			default:
				super.handleMessage(msg);
			}
		}
	}


	private void sendMessageToUI(int message) {
		for (int i=mClients.size()-1; i>=0; i--) {
			try {
				// Send message value
				mClients.get(i).send(Message.obtain(null, message));
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}
	
	private void sendAccelValuesToUI(float accX, float accY, float accZ) {
		for (int i=mClients.size()-1; i>=0; i-- ) {
			try {
				
				//Send Accel Values
				Bundle b = new Bundle();
				b.putFloat("accx", accX);
				b.putFloat("accy", accY);
				b.putFloat("accz", accZ);
				Message msg = Message.obtain(null, MSG_ACCEL_VALUES);
				msg.setData(b);
				mClients.get(i).send(msg);

			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}
	
	private void sendUpdatedStepCountToUI() {
		for (int i=mClients.size()-1; i>=0; i-- ) {
			try {
				//Send Step Count
				Message msg = Message.obtain(null, MSG_STEP_COUNTER,stepCount,0);
				mClients.get(i).send(msg);

			} catch (RemoteException e) {
				// The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
				mClients.remove(i);
			}
		}
	}

    private String activityChecker(int number)
    {
        String realActivity = "";
        if(number == 1)
        {
            realActivity = "Walking";
        } else if(number == 2)
        {
            realActivity = "Running";
        }
        else if (number == 3)
        {
            realActivity = "";
        }
        else
        {
            realActivity = "Still";
        }
        return realActivity;
    }

    private String voiceChecker(int number)
    {
        String realSpeech = "";
        if(number == 1)
        {
            realSpeech = "Speech";
        } else if(number == 0)
        {
            realSpeech = "No Speech";
        }

        return realSpeech;
    }
	private void sendActivityToUI(int number)
    {


        for (int i=mClients.size()-1; i>=0; i-- ) {
            try {
                //Send Step Count
                Bundle bundle = new Bundle();
                bundle.putString("activity",activityChecker(number));
                Message msg = Message.obtain(null, MSG_ACTIVITY_STATUS);
                msg.setData(bundle);
                mClients.get(i).send(msg);


            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }

    }
    private void sendSpeechToUI(int number)
    {


        for (int i=mClients.size()-1; i>=0; i-- ) {
            try {
                //Send Step Count
                Bundle bundle = new Bundle();
                bundle.putString("speech",voiceChecker(number));
                Message msg = Message.obtain(null, MSG_MICROPHONE_STATUS);
                msg.setData(bundle);
                mClients.get(i).send(msg);


            } catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }

    }
	

	/**
	 * On Binding, return a binder
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}


	
	

	//Start service automatically if we reboot the phone
	public static class Context_BGReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Intent bootUp = new Intent(context,Context_Service.class);
			context.startService(bootUp);
		}		
	}

	@SuppressWarnings("deprecation")
	private void showNotification() {
		//Cancel previous notification
		if(nm!=null)
			nm.cancel(NOTIFICATION_ID);
		else
			nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

		// The PendingIntent to launch our activity if the user selects this notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);

		// Use the commented block of code if your target environment is Android-16 or higher 
		/*Notification notification = new Notification.Builder(this)
		.setContentTitle("Context Service")
		.setContentText("Running").setSmallIcon(R.drawable.icon)
		.setContentIntent(contentIntent)
		.build();
		
		nm.notify(NOTIFICATION_ID, notification); */
		
		//For lower versions of Android, the following code should work
		Notification notification = new Notification();
		notification.icon = R.drawable.icon;
		notification.tickerText = getString(R.string.app_name);
		notification.contentIntent = contentIntent;
        notification.when = System.currentTimeMillis();
        if(isAccelerometerRunning())
        	notification.setLatestEventInfo(getApplicationContext(), getString(R.string.app_name), "Accelerometer Running", contentIntent);
        else
        	notification.setLatestEventInfo(getApplicationContext(), getString(R.string.app_name), "Accelerometer Not Started", contentIntent);

        if(isMicrophoneRunning())
            notification.setLatestEventInfo(getApplicationContext(), getString(R.string.app_name), "Microphone Running", contentIntent);
        else
            notification.setLatestEventInfo(getApplicationContext(), getString(R.string.app_name), "Microphone Not Started", contentIntent);
        
        // Send the notification.
        nm.notify(NOTIFICATION_ID, notification);
	}

	/* getInstance() and isRunning() are required by the */
	static Context_Service getInstance(){
		return sInstance;
	}

	protected static boolean isRunning(){
		return isRunning;
	}
	
	protected static boolean isAccelerometerRunning() {
		return isAccelRunning;
	}
    protected static boolean isMicrophoneRunning() {
        return isMicrophoneRunning;
    }

	@Override
	public void onCreate() {
		super.onCreate();
		showNotification();
		isRunning = true;
		sInstance = this;
        mAccBuffer = new ArrayBlockingQueue<Double>(
               2048);
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);



	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		nm.cancel(NOTIFICATION_ID); // Cancel the persistent notification.
        isRunning = false;
		//Don't let Context_Service die!
		Intent mobilityIntent = new Intent(this,Context_Service.class);
		startService(mobilityIntent);
	}

	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Create the container for attributes
        ArrayList<Attribute> allAttr = new ArrayList<Attribute>();

        // Adding FFT coefficient attributes
        DecimalFormat df = new DecimalFormat("0000");

        for (int i = 0; i < Globals.ACCELEROMETER_BLOCK_CAPACITY; i++) {
            allAttr.add(new Attribute(Globals.FEAT_FFT_COEF_LABEL + df.format(i)));
        }
        // Adding the max feature
        allAttr.add(new Attribute(Globals.FEAT_MAX_LABEL));

        // Declare a nominal attribute along with its candidate values
        ArrayList<String> labelItems = new ArrayList<String>(3);
        labelItems.add(Globals.CLASS_LABEL_STANDING);
        labelItems.add(Globals.CLASS_LABEL_WALKING);
        labelItems.add(Globals.CLASS_LABEL_RUNNING);
        labelItems.add(Globals.CLASS_LABEL_OTHER);
        labelItems.add(Globals.CLASS_LABEL_SITTING);
        mClassAttribute = new Attribute(Globals.CLASS_LABEL_KEY, labelItems);
        allAttr.add(mClassAttribute);

        // Construct the dataset with the attributes specified as allAttr and
        // capacity 10000
        mDataset = new Instances(Globals.FEAT_SET_NAME, allAttr, Globals.FEATURE_SET_CAPACITY);

        // Set the last column/attribute (standing/walking/running) as the class
        // index for classification
        mDataset.setClassIndex(mDataset.numAttributes() - 1);

        mAsyncTask = new OnSensorChangedTask();
        mAsyncTask.execute();
        return START_STICKY; // run until explicitly stopped.
    }


	/* (non-Javadoc)
	 * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
	 */
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		
	}

	/* (non-Javadoc)
	 * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
			
			float accel[] = event.values;
			sendAccelValuesToUI(accel[0], accel[1], accel[2]);

			
			
			/**
			 * TODO: Step Detection
			 */

			//First, Get filtered values
			double filtAcc[] = filter.getFilteredValues(accel[0], accel[1], accel[2]);
           // double filtAcc[] = filter.highPassFilter(accel[0], accel[1], accel[2]);

            findSteps(filtAcc[0], filtAcc[1], filtAcc[2]);
            //findSteps(accel[0], accel[1], accel[2]);
            ActivityData(accel[0], accel[1], accel[2]);

            //Now, increment 'stepCount' variable if you detect any steps here
			//stepCount += detectSteps(0);
			//detectSteps() is not implemented 
			sendUpdatedStepCountToUI();
			
		}

	}
	

	public int detectSteps(int step) {



		return step;
	}

    public void findSteps(double filt_acc_x, double filt_acc_y, double filt_acc_z)
    {
       filteredNumbers.add(Math.sqrt(Math.pow(filt_acc_x,2) + Math.pow(filt_acc_y,2) + Math.pow(filt_acc_z,2)));
        Numbers.add (Math.sqrt(Math.pow(filt_acc_x,2) + Math.pow(filt_acc_y,2) + Math.pow(filt_acc_z,2)));
         if(Numbers.size() == 100)
        {

           ArrayList<Double> Smothedwindow = exponentialSmothing(Numbers,0.003);
            Double average = (Collections.max(Smothedwindow,null)+ Collections.min(Smothedwindow,null))/2;
            if(average > 1.4){
                for (int l = 0; l < (Smothedwindow.size() - 1); l++) {
                    double numberOne = Smothedwindow.get(l);
                    double numberTwo = Smothedwindow.get(l + 1);


                    //if(numberTwo < 0 && numberOne > 0)
                    if (numberTwo < average && numberOne > average) {
                        stepCount += detectSteps(1);
                    }
                }
            }
            Numbers = new ArrayList<>();
        }

    }

    public ArrayList<Double> exponentialSmothing(ArrayList<Double> s,double alpha)
    {
        ArrayList<Double> smothed = new ArrayList<Double>(Collections.nCopies(s.size(), 0.0));
        smothed.set(1, s.get(1));
        for(int l=3;l<(s.size()+1);l++)
        {
            try {
                double v = alpha * s.get(l - 1) + (1 - alpha)*smothed.get(l - 1);
                smothed.set(l-1,v);
            } catch (Exception e)
            {
                e.printStackTrace();
                Log.e("error",l+"");
            }

        }

        return smothed;
    }

    public void ActivityData(double filt_acc_x, double filt_acc_y, double filt_acc_z)
    {

        double m = Math.sqrt(filt_acc_x * filt_acc_x
                + filt_acc_y * filt_acc_y + filt_acc_z * filt_acc_z);

        try {
            mAccBuffer.add(new Double(m));
        } catch (IllegalStateException e) {

            // Exception happens when reach the capacity.
            // Doubling the buffer. ListBlockingQueue has no such issue,
            // But generally has worse performance
            ArrayBlockingQueue<Double> newBuf = new ArrayBlockingQueue<Double>(
                    mAccBuffer.size() * 2);

            mAccBuffer.drainTo(newBuf);
            mAccBuffer = newBuf;
            mAccBuffer.add(new Double(m));
        }
    }
    private class OnSensorChangedTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... arg0) {

            Instance inst = new DenseInstance(mFeatLen);
            inst.setDataset(mDataset);
            int blockSize = 0;
            double num = 0.0;
            FFT fft = new FFT(Globals.ACCELEROMETER_BLOCK_CAPACITY);
            double[] accBlock = new double[Globals.ACCELEROMETER_BLOCK_CAPACITY];
            double[] re = accBlock;
            double[] im = new double[Globals.ACCELEROMETER_BLOCK_CAPACITY];

            double max = Double.MIN_VALUE;

            while (true) {
                try {
                    // need to check if the AsyncTask is cancelled or not in the while loop
                    if (isCancelled() == true) {
                        return null;
                    }

                    // Dumping buffer
                    accBlock[blockSize++] = mAccBuffer.take().doubleValue();

                    if (blockSize == Globals.ACCELEROMETER_BLOCK_CAPACITY) {
                        blockSize = 0;

                        // time = System.currentTimeMillis();
                        max = .0;
                        for (double val : accBlock) {
                            if (max < val) {
                                max = val;
                            }
                        }

                        fft.fft(re, im);

                        for (int i = 0; i < re.length; i++) {
                            double mag = Math.sqrt(re[i] * re[i] + im[i]
                                    * im[i]);
                            inst.setValue(i, mag);
                            im[i] = .0; // Clear the field
                        }

                        // Append max after frequency component
                        inst.setValue(Globals.ACCELEROMETER_BLOCK_CAPACITY, max);
                        //inst.setValue(mClassAttribute, "");
                        mDataset.add(inst);
                        double[] v=inst.toDoubleArray();
                        Double[] array = new Double[v.length];
                        for(int z=0;z <v.length;z++)
                        {
                            array[z] = Double.valueOf(v[z]);
                        }

                        try {
                            num = WekaClassifier.classify(array);
                            dispatcher(num);
                        }catch (Exception e)
                        {
                           e.printStackTrace();

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }

    @Override
    //This method was declared in an interface in MicrophoneRecorder
    public void microphoneBuffer(short[] buffer, int window_size) {

        //You will break a chunk of one-second-long samples into multiple 25-ms windows. Think about how many of 25-ms windows you will get for a second.

        //number of 25-ms-long windows that contains mostly voice in a second
        int voiced = 0;
        int unVoiced = 0;

        //TODO: Fill out appropriate numbers in the following for loop statement

            //TODO: replace ‘??’ with appropriate numbers below
        for(int k=0;k<1000;k+=25){

            double[] features = FeatureExtractor.ComputeFeaturesForFrame(buffer, 25, k);
            try {
                result = getObjectDoubleArray(features, result);
                //TODO: classify whether the window is voiced or not
                //If output of the classifier is 0.0d, increment ‘voiced’ variable. If output is 1.0d, it is unvoiced. This is assuming that you have the order of classes written in arff file as: “speech{true,false}.”
                speechNumber = (int) Math.round(SpeechClassifier.classify(result));

                //Log.d("speech",speechNumber+"");


                if(speechNumber == 0)
                {
                    unVoiced += 1;
                }
                else if (speechNumber == 1)
                {
                    voiced += 1;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

         if(voiced > unVoiced)
         {
             sendSpeechToUI(1);
             Log.d("Voiced",voiced+"");
             Log.d("Unvoiced",unVoiced+"");
         }
        else if(voiced < unVoiced)
         {
             sendSpeechToUI(0);

         }

        //TODO: After you find the number of ‘voiced’ windows, determine whether a-second-long audio mostly contain human voice or not by thresholding. Test out different thresholds and choose whichever works the best. Keep in mind that threshold should be less than the number of 25-ms-long windows that contain in a second. If ‘voiced’ variable is greater than a certain threshold, call sendSpeechStatusToUI() with speech variable=1.
    }

    public static Object[] getObjectDoubleArray(double[] in,Object[] result){
        for (int i=0;i<in.length;i++){
            result[i] = new Double(in[i]);
        }
        return result;
    }


    public void dispatcher(double num) {

        int foo = (int) Math.round(num);

        sendActivityToUI(foo);
      /*  currentState = foo;

        if (currentState == previousState) {
            increment = increment + 1;
            previousState = currentState;
        } else {
            increment = 0;
            previousState = currentState;
        }

        if (increment > 0) {
            sendActivityToUI(currentState);
            increment = 0;
        }*/



/*
        if(currentState != foo)
        {
          String newState = activityChecker(foo);
          String oldState = activityChecker(currentState);


          Log.d("State Change","Changed from "+ currentState + oldState + " to "+ foo + newState );
            currentState = foo;

        }
*/


        //Log.d("current string",foo+"");


    }

}


