package edu.umass.cs.client;

/**
 * Created by tmutabazi on 9/25/2015.
 */
public class WekaClassifier {

    public static double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifier.N41ca3ab0(i);
        return p;
    }
    static double N41ca3ab0(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 4;
        } else if (((Double) i[0]).doubleValue() <= 107.213284) {
            p = WekaClassifier.Ndb111931(i);
        } else if (((Double) i[0]).doubleValue() > 107.213284) {
            p = WekaClassifier.N74ab5fe019(i);
        }
        return p;
    }
    static double Ndb111931(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 4;
        } else if (((Double) i[0]).doubleValue() <= 6.162051) {
            p = WekaClassifier.N714c5f1d2(i);
        } else if (((Double) i[0]).doubleValue() > 6.162051) {
            p = WekaClassifier.Nf4977c84(i);
        }
        return p;
    }
    static double N714c5f1d2(Object []i) {
        double p = Double.NaN;
        if (i[5] == null) {
            p = 4;
        } else if (((Double) i[5]).doubleValue() <= 0.647656) {
            p = 4;
        } else if (((Double) i[5]).doubleValue() > 0.647656) {
            p = WekaClassifier.N6ea9904d3(i);
        }
        return p;
    }
    static double N6ea9904d3(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() <= 0.576839) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() > 0.576839) {
            p = 4;
        }
        return p;
    }
    static double Nf4977c84(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 0.307072) {
            p = WekaClassifier.Naf1c23b5(i);
        } else if (((Double) i[64]).doubleValue() > 0.307072) {
            p = WekaClassifier.N28c3334817(i);
        }
        return p;
    }
    static double Naf1c23b5(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 0;
        } else if (((Double) i[2]).doubleValue() <= 1.328406) {
            p = WekaClassifier.N1956d0cf6(i);
        } else if (((Double) i[2]).doubleValue() > 1.328406) {
            p = WekaClassifier.N111401f15(i);
        }
        return p;
    }
    static double N1956d0cf6(Object []i) {
        double p = Double.NaN;
        if (i[3] == null) {
            p = 0;
        } else if (((Double) i[3]).doubleValue() <= 1.098038) {
            p = WekaClassifier.N3bdf10507(i);
        } else if (((Double) i[3]).doubleValue() > 1.098038) {
            p = WekaClassifier.N5ab1a29b14(i);
        }
        return p;
    }
    static double N3bdf10507(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 0;
        } else if (((Double) i[6]).doubleValue() <= 0.567673) {
            p = WekaClassifier.N7f6cd7d48(i);
        } else if (((Double) i[6]).doubleValue() > 0.567673) {
            p = 0;
        }
        return p;
    }
    static double N7f6cd7d48(Object []i) {
        double p = Double.NaN;
        if (i[9] == null) {
            p = 0;
        } else if (((Double) i[9]).doubleValue() <= 0.207716) {
            p = WekaClassifier.N7b8822189(i);
        } else if (((Double) i[9]).doubleValue() > 0.207716) {
            p = WekaClassifier.N17e9924011(i);
        }
        return p;
    }
    static double N7b8822189(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 4;
        } else if (((Double) i[64]).doubleValue() <= 0.208404) {
            p = 4;
        } else if (((Double) i[64]).doubleValue() > 0.208404) {
            p = WekaClassifier.N5477550e10(i);
        }
        return p;
    }
    static double N5477550e10(Object []i) {
        double p = Double.NaN;
        if (i[16] == null) {
            p = 4;
        } else if (((Double) i[16]).doubleValue() <= 0.156816) {
            p = 4;
        } else if (((Double) i[16]).doubleValue() > 0.156816) {
            p = 0;
        }
        return p;
    }
    static double N17e9924011(Object []i) {
        double p = Double.NaN;
        if (i[30] == null) {
            p = 0;
        } else if (((Double) i[30]).doubleValue() <= 0.0942) {
            p = WekaClassifier.Ne22251e12(i);
        } else if (((Double) i[30]).doubleValue() > 0.0942) {
            p = 0;
        }
        return p;
    }
    static double Ne22251e12(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 4;
        } else if (((Double) i[8]).doubleValue() <= 0.278049) {
            p = WekaClassifier.N34804e2013(i);
        } else if (((Double) i[8]).doubleValue() > 0.278049) {
            p = 0;
        }
        return p;
    }
    static double N34804e2013(Object []i) {
        double p = Double.NaN;
        if (i[10] == null) {
            p = 4;
        } else if (((Double) i[10]).doubleValue() <= 0.484605) {
            p = 4;
        } else if (((Double) i[10]).doubleValue() > 0.484605) {
            p = 0;
        }
        return p;
    }
    static double N5ab1a29b14(Object []i) {
        double p = Double.NaN;
        if (i[25] == null) {
            p = 4;
        } else if (((Double) i[25]).doubleValue() <= 0.221816) {
            p = 4;
        } else if (((Double) i[25]).doubleValue() > 0.221816) {
            p = 0;
        }
        return p;
    }
    static double N111401f15(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 4;
        } else if (((Double) i[7]).doubleValue() <= 0.573409) {
            p = WekaClassifier.N1e848ff616(i);
        } else if (((Double) i[7]).doubleValue() > 0.573409) {
            p = 0;
        }
        return p;
    }
    static double N1e848ff616(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 4;
        } else if (((Double) i[8]).doubleValue() <= 0.599857) {
            p = 4;
        } else if (((Double) i[8]).doubleValue() > 0.599857) {
            p = 0;
        }
        return p;
    }
    static double N28c3334817(Object []i) {
        double p = Double.NaN;
        if (i[32] == null) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() <= 0.69074) {
            p = 0;
        } else if (((Double) i[32]).doubleValue() > 0.69074) {
            p = WekaClassifier.N7fb1f9b118(i);
        }
        return p;
    }
    static double N7fb1f9b118(Object []i) {
        double p = Double.NaN;
        if (i[8] == null) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() <= 1.243365) {
            p = 0;
        } else if (((Double) i[8]).doubleValue() > 1.243365) {
            p = 4;
        }
        return p;
    }
    static double N74ab5fe019(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 1;
        } else if (((Double) i[64]).doubleValue() <= 36.313206) {
            p = WekaClassifier.N4e6a95bc20(i);
        } else if (((Double) i[64]).doubleValue() > 36.313206) {
            p = WekaClassifier.N2b248a3a26(i);
        }
        return p;
    }
    static double N4e6a95bc20(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 216.834671) {
            p = WekaClassifier.N7f82b64a21(i);
        } else if (((Double) i[0]).doubleValue() > 216.834671) {
            p = WekaClassifier.Nf0ea47a22(i);
        }
        return p;
    }
    static double N7f82b64a21(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() <= 30.501498) {
            p = 1;
        } else if (((Double) i[2]).doubleValue() > 30.501498) {
            p = 4;
        }
        return p;
    }
    static double Nf0ea47a22(Object []i) {
        double p = Double.NaN;
        if (i[6] == null) {
            p = 1;
        } else if (((Double) i[6]).doubleValue() <= 41.640132) {
            p = WekaClassifier.N2174ae2423(i);
        } else if (((Double) i[6]).doubleValue() > 41.640132) {
            p = WekaClassifier.N5173c91a25(i);
        }
        return p;
    }
    static double N2174ae2423(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 944.256921) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 944.256921) {
            p = WekaClassifier.N3ba918c324(i);
        }
        return p;
    }
    static double N3ba918c324(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() <= 217.716379) {
            p = 2;
        } else if (((Double) i[1]).doubleValue() > 217.716379) {
            p = 1;
        }
        return p;
    }
    static double N5173c91a25(Object []i) {
        double p = Double.NaN;
        if (i[2] == null) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() <= 88.485829) {
            p = 2;
        } else if (((Double) i[2]).doubleValue() > 88.485829) {
            p = 1;
        }
        return p;
    }
    static double N2b248a3a26(Object []i) {
        double p = Double.NaN;
        if (i[7] == null) {
            p = 1;
        } else if (((Double) i[7]).doubleValue() <= 10.515694) {
            p = WekaClassifier.N2fa514f427(i);
        } else if (((Double) i[7]).doubleValue() > 10.515694) {
            p = 2;
        }
        return p;
    }
    static double N2fa514f427(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 1239.37683) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 1239.37683) {
            p = 2;
        }
        return p;
    }
}
